# Required modules ###
require ifcdaq,0.2.0+
require singlemotion,1.4.6+
require mrfioc2,2.7.13
require pev,0.1.1
require ecat2db,0.4.3
require FastAcquisition,1.0.4-catania
require DataAcquisition,1.1.0
#require emu-motor-test,1.0.0
require tpmac,3.11.2+
require PVArchiving,1.0.2
require emu,1.0.5


### MACROS ###
# PMAC common macros3       ) 
epicsEnvSet("PMACPREFIX", "LNS-ISRC-010:PBI-EM-PMAC")
epicsEnvSet("PMACPORT",   "GEOBRICK_ASYN")
epicsEnvSet("MOTOR_PORT", "pmac1" )
epicsEnvSet("NB_AXES", "2" )

# ARCHIVE macros #
########################################## epicsEnvSet("ARCHIVE-MACRO-EMUV","LNS-ISRC-010:PBI-EMV")
epicsEnvSet("ARCHIVE-MACRO-EMUH","LNS-ISRC-010:PBI-EMH")

# Motor Macros
epicsEnvSet("EGU",       "mm"   )
epicsEnvSet("DIR",       "Pos"  )
epicsEnvSet("MRES",      "0.000249515")
epicsEnvSet("PREC",      "4"    )
epicsEnvSet("DHLM",      "4"   )
epicsEnvSet("DLLM",      "-200"  )
epicsEnvSet("VELO",      "1.5"    )
epicsEnvSet("HVEL",      "1.5"    )
epicsEnvSet("VBAS",      "0.1"  )
epicsEnvSet("VMAX",      "7"    )
epicsEnvSet("ACCL",      "5"  )
epicsEnvSet("BDST",      "0"    )
epicsEnvSet("INIT",      ""     )
epicsEnvSet("OFF",       "0"    )

########################################## epicsEnvSet("MOTOR_NAME1",  "MTR1")
########################################## epicsEnvSet("AXIS_NO1",     "1")
epicsEnvSet("MOTOR_NAME2",  "MTR2")
epicsEnvSet("AXIS_NO2",     "2")

# Motor Status macros
epicsEnvSet("SCAN",         "1 second")
epicsEnvSet("OVERHEAT1",    "MAJOR"   )
epicsEnvSet("SWITCH_OFF1",  "MINOR"   )
epicsEnvSet("OVERHEAT2",    "0"       )
epicsEnvSet("SWITCH_OFF2",  "0"       )
epicsEnvSet("MOTOR_ERROR1", "0"       )
epicsEnvSet("MOTOR_ERROR2", "0"       )
epicsEnvSet("MOTOR_ERROR3", "0"       )
epicsEnvSet("MOTOR_ERROR4", "0"       )

# EVG Macros 
epicsEnvSet("SYS"             "SYS0")
#####NOT NEEDED AT CATANIA epicsEnvSet("EVG"             "EVG0")
epicsEnvSet("EVG_VMESLOT"     "8")

# EVR Macros
epicsEnvSet("EVR_PCIDOMAIN",    "0000")
epicsEnvSet("EVR_PCIBUS",       "05")
epicsEnvSet("EVR_PCIDEVICE",    "00")
epicsEnvSet("EVR_PCIFUNCTION",  "0")
epicsEnvSet("EVR",              "EVR0")
epicsEnvSet("EVR_PUL0_EVENT",   "14")
epicsEnvSet("EVR_PUL0_DELAY",   "$(EVR_EV14_OUT0_DELAY=0)")
epicsEnvSet("EVR_PUL0_WIDTH",   "$(EVR_EV14_OUT0_WIDTH=1000)")

#DAQ macros
epicsEnvSet("DAQPREFIX",  "LNS-ISRC-010"  )
epicsEnvSet("DAQBUFSIZE", "1024"         )
epicsEnvSet("BUFFERSIZE", "40000"         )

#Scanning macros
epicsEnvSet("SCANPREFIX_EMU_V", "LNS-ISRC-010:PBI-EMV")
epicsEnvSet("SCANPREFIX_EMU_H", "LNS-ISRC-010:PBI-EMH")
epicsEnvSet("SCAN_SLIT", "MTR"          )
epicsEnvSet("SCAN_EF", "PS"              )
epicsEnvSet("SCAN_POINTS_EF", "400"     )
epicsEnvSet("SCAN_POINTS_SLIT", "100"    )

epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 8000000)

# Connection to PMAC and setup for $(NUM_MOTORS) motors
pmacAsynIPConfigure($(PMACPORT), "10.10.3.42:1025")
pmacCreateController($(MOTOR_PORT), $(PMACPORT), 0, $(NB_AXES), 50, 300)
### Motor Vertical
########################################## pmacCreateAxis($(MOTOR_PORT), $(AXIS_NO1))
### Motor Horizontal
pmacCreateAxis($(MOTOR_PORT), $(AXIS_NO2))

# Initialize EVR
mrmEvrSetupPCI($(EVR), $(EVR_PCIDOMAIN), $(EVR_PCIBUS), $(EVR_PCIDEVICE), $(EVR_PCIFUNCTION))

# Initialize daq
ndsCreateDevice(ifcdaq, CARD0, card=0, fmc=2)

### LOAD RECORDS ###

######### EVR #########
dbLoadRecords("evr-pmc-230.db", "DEVICE=$(EVR), SYS=$(SYS)")
dbLoadRecords("evr-softEvent.template", "DEVICE=$(EVR), SYS=$(SYS), EVT=$(EVR_PUL0_EVENT), CODE=$(EVR_PUL0_EVENT)")
dbLoadRecords("evr-pulserMap.template", "DEVICE=$(EVR), SYS=$(SYS), EVT=$(EVR_PUL0_EVENT), PID=0, F=Trig, ID=0")


######### MOTORS #########
# EMU vertical
########################################## dbLoadRecords("motor.template", "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),MOTOR_PORT=$(MOTOR_PORT),AXIS_NO=$(AXIS_NO1),EGU=$(EGU),DIR=$(DIR),MRES=$(MRES),PREC=$(PREC),DHLM=$(DHLM),DLLM=$(DLLM),HVEL=$(HVEL),VELO=$(VELO),VBAS=$(VBAS),VMAX=$(VMAX),ACCL=$(ACCL),BDST=$(BDST),INIT=$(INIT),OFF=$(OFF)")

# EMU horizontal
dbLoadRecords("motor.template", "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME2),MOTOR_PORT=$(MOTOR_PORT),AXIS_NO=$(AXIS_NO2),EGU=$(EGU),DIR=$(DIR),MRES=$(MRES),PREC=$(PREC),DHLM=$(DHLM),DLLM=$(DLLM),HVEL=$(HVEL),VELO=$(VELO),VBAS=$(VBAS),VMAX=$(VMAX),ACCL=$(ACCL),BDST=$(BDST),INIT=$(INIT),OFF=$(OFF)")


######### MOTORS STATUS  #########
# EMU vertical
########################################## dbLoadRecords("motorStatus.template", "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),ASYN_PORT=$(PMACPORT),AXIS_NO=$(AXIS_NO1),SCAN=$(SCAN),EGU=$(EGU),PREC=$(PREC),OVERHEAT1=$(OVERHEAT1),SWITCH_OFF1=$(SWITCH_OFF1),OVERHEAT2=$(OVERHEAT2),SWITCH_OFF2=$(SWITCH_OFF2),MOTOR_ERROR1=$(MOTOR_ERROR1),MOTOR_ERROR2=$(MOTOR_ERROR2),MOTOR_ERROR3=$(MOTOR_ERROR3),MOTOR_ERROR4=$(MOTOR_ERROR4)")

# EMU horizontal
dbLoadRecords("motorStatus.template", "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME2),ASYN_PORT=$(PMACPORT),AXIS_NO=$(AXIS_NO2),SCAN=$(SCAN),EGU=$(EGU),PREC=$(PREC),OVERHEAT1=$(OVERHEAT1),SWITCH_OFF1=$(SWITCH_OFF1),OVERHEAT2=$(OVERHEAT2),SWITCH_OFF2=$(SWITCH_OFF2),MOTOR_ERROR1=$(MOTOR_ERROR1),MOTOR_ERROR2=$(MOTOR_ERROR2),MOTOR_ERROR3=$(MOTOR_ERROR3),MOTOR_ERROR4=$(MOTOR_ERROR4)")

######### HOMING PROCEDURE #########
# EMU vertical
########################################## dbLoadRecords("motorHoming.template", "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),ASYN_PORT=$(PMACPORT),AXIS_NO=$(AXIS_NO1),PREC=$(PREC),EGU=$(EGU)")

# EMU horizontal
dbLoadRecords("motorHoming.template", "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME2),ASYN_PORT=$(PMACPORT),AXIS_NO=$(AXIS_NO2),PREC=$(PREC),EGU=$(EGU)")

######### EMU #########
# common database
dbLoadRecords("EMU_VME_common.db","DEVICE=LNS-ISRC-010:PBI-EM")

# EMU vertical
########################################## dbLoadRecords("EMU_VME.db", "DEVICE=$(SCANPREFIX_EMU_V), SCAN_SLIT=$(SCAN_SLIT), SCAN_EF=$(SCAN_EF),MTRREC=$(PMACPREFIX):$(MOTOR_NAME1), MAX_POINTS_EF=$(SCAN_POINTS_EF), MAX_POINTS_SLIT=$(SCAN_POINTS_SLIT), DAQNDSPREFIX=$(DAQPREFIX), DAQBUFSIZE=$(DAQBUFSIZE), DET1WF=PBI-EMV-FC:CurrR_Y, DET2WF=PBI-EM-HV1:VoltR_Y, DET3WF=PBI-EM-HV2:VoltR_Y, TR_TL=$(SYS)-$(EVR):Pul0-Ena-Sel, TR_TLTSEL=$(SYS)-$(EVR):Event-$(EVR_PUL0_EVENT)-SP.TIME, PSU_SP=LNS-ISRC-010:PBI-EM:HVhor-SP, PSU_RBV=LNS-ISRC-010:PBI-EM:HVhor-SP, PSU2_SP=LNS-ISRC-010:PBI-EM:HVver-SP, PSU2_RBV=LNS-ISRC-010:PBI-EM:HVver-SP,BUFFERSIZE=$(BUFFERSIZE)")

# EMU horizontal
dbLoadRecords("EMU_VME.db", "DEVICE=$(SCANPREFIX_EMU_H), SCAN_SLIT=$(SCAN_SLIT), SCAN_EF=$(SCAN_EF),MTRREC=$(PMACPREFIX):$(MOTOR_NAME2), MAX_POINTS_EF=$(SCAN_POINTS_EF), MAX_POINTS_SLIT=$(SCAN_POINTS_SLIT), DAQNDSPREFIX=$(DAQPREFIX), DAQBUFSIZE=$(DAQBUFSIZE), DET1WF=PBI-EMH-FC:CurrR_Y, DET2WF=PBI-EM-HV1:VoltR_Y, DET3WF=PBI-EM-HV2:VoltR_Y, TR_TL=$(SYS)-$(EVR):Pul0-Ena-Sel, TR_TLTSEL=$(SYS)-$(EVR):Event-$(EVR_PUL0_EVENT)-SP.TIME, PSU_SP=LNS-ISRC-010:PBI-EM:HVhor-SP, PSU_RBV=LNS-ISRC-010:PBI-EM:HVhor-SP, PSU2_SP=LNS-ISRC-010:PBI-EM:HVver-SP, PSU2_RBV=LNS-ISRC-010:PBI-EM:HVver-SP,BUFFERSIZE=$(BUFFERSIZE)")

#
# Init ETHERCAT module
#

ecat2configure(0,500,1,1)

#
# Archive module
#

ArchiveConfigure("$(REQUIRE_PVArchiving_PATH)","10.10.0.11:17665")
########################################## dbLoadRecords("PVArchiving.template",PREFIX=$(ARCHIVE-MACRO-EMUV))
dbLoadRecords("PVArchiving.template",PREFIX=$(ARCHIVE-MACRO-EMUH))

#############################################################################
######################### iocInit ###########################################
#############################################################################
iocInit

#### Motor homing
########################################## seq motorHoming "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1)"
seq motorHoming "PREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME2)"
########################################## seq homeMtrAuto "PMACPREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),DEVICE=$(SCANPREFIX_EMU_V),PREFIX=LNS-ISRC-010"
seq homeMtrAuto "PMACPREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME2),DEVICE=$(SCANPREFIX_EMU_H),PREFIX=LNS-ISRC-010"
########################################## seq checkMTRBrake "PMACPREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),DEVICE=$(SCANPREFIX_EMU_V),PREFIX=LNS-ISRC-010"
seq checkMTRBrake "PMACPREFIX=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME2),DEVICE=$(SCANPREFIX_EMU_H),PREFIX=LNS-ISRC-010"

# SET INTERFACE FROM IFCDAQ TO Allison scanner
# Forward link wf record to Detector guard
# EMU vertical
########################################## dbpf $(DAQPREFIX):PBI-EMV-FC:CurrR_Y.FLNK $(SCANPREFIX_EMU_V):DET1-GUARD
########################################## dbpf $(DAQPREFIX):PBI-EM-HV1:VoltR_Y.FLNK $(SCANPREFIX_EMU_V):DET2-GUARD
########################################## dbpf $(DAQPREFIX):PBI-EM-HV2:VoltR_Y.FLNK $(SCANPREFIX_EMU_V):DET3-GUARD
# EMU horizontal
dbpf $(DAQPREFIX):PBI-EMH-FC:CurrR_Y.FLNK $(SCANPREFIX_EMU_H):DET1-GUARD
dbpf $(DAQPREFIX):PBI-EM-HV1:VoltR_Y.FLNK $(SCANPREFIX_EMU_H):DET2-GUARD
dbpf $(DAQPREFIX):PBI-EM-HV2:VoltR_Y.FLNK $(SCANPREFIX_EMU_H):DET3-GUARD

# Forward link from state record to Detector trigger
########################################## dbpf $(DAQPREFIX):PBI-EM:CARD0-STAT-RB.FLNK $(SCANPREFIX_EMU_V):DAQTRG-DAQSTGUARD
dbpf $(DAQPREFIX):PBI-EM:CARD0-STAT-RB.FLNK $(SCANPREFIX_EMU_H):DAQTRG-DAQSTGUARD


# Forward link number of samples rbv to compress N record, so that detetor know number of elements to average over.
########################################## dbpf $(DAQPREFIX):PBI-EM:CARD0:NSAMPLES-RB.FLNK "$(SCANPREFIX_EMU_V):DET1-COMPRESSN"
dbpf $(DAQPREFIX):PBI-EM:CARD0:NSAMPLES-RB.FLNK "$(SCANPREFIX_EMU_H):DET1-COMPRESSN"


# Set tsel on WF record to event record in EVR
########################################## dbpf $(DAQPREFIX):PBI-EMV-FC:CurrR_Y.TSEL $(SYS)-$(EVR):Event-$(EVR_PUL0_EVENT)-SP.TIME
dbpf $(DAQPREFIX):PBI-EMH-FC:CurrR_Y.TSEL $(SYS)-$(EVR):Event-$(EVR_PUL0_EVENT)-SP.TIME
dbpf $(DAQPREFIX):PBI-EM-HV1:VoltR_Y.TSEL $(SYS)-$(EVR):Event-$(EVR_PUL0_EVENT)-SP.TIME
dbpf $(DAQPREFIX):PBI-EM-HV2:VoltR_Y.TSEL $(SYS)-$(EVR):Event-$(EVR_PUL0_EVENT)-SP.TIME
dbpf $(SYS)-$(EVR):Time-I.TSE -2

############################################################
################### Configuration Timing ###################
############################################################

### Setup EVR
##set the pulser 0 to trigger on output 0
########## TIMING RECEIVER: setup OUT0 ##############
dbpf $(SYS)-$(EVR):Link-Clk-SP           88.0519    
dbpf $(SYS)-$(EVR):FrontOut0-Src-SP      0
dbpf $(SYS)-$(EVR):Pul0-Evt-Trig0-SP 	 $(EVR_PUL0_EVENT)
dbpf $(SYS)-$(EVR):Pul0-Width-SP         20000
dbpf $(SYS)-$(EVR):Pul0-Delay-SP         0
dbpf $(SYS)-$(EVR):FrontOut0-Ena-SP      "Enabled"

### Setup IFCDAQ PVs ###
# Set trigger repeat to 1
dbpf $(DAQPREFIX):PBI-EM:CARD0:TriggerRepeat 1
# Set trigger source to external GPIO
dbpf $(DAQPREFIX):PBI-EM:CARD0:TRIGGERSOURCE "EXT-GPIO"
sleep(1)
dbpf $(DAQPREFIX):PBI-EM:CARD0-STAT ON
sleep(3)
dbpf $(DAQPREFIX):PBI-EM:CARD0-STAT RUNNING
sleep(1)
dbpf $(DAQPREFIX):PBI-EM:CARD0-STAT RUNNING
# Set the Frequency to 1MHz
dbpf $(DAQPREFIX):PBI-EM:CARD0:SAMPLINGRATE 1000000

### Setup reasonable values for sscan record ###
# Initialize Motor positioners vertical
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).P4SP -105
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).P4EP -195
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).P2SP 1.5
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).P2EP 1.5
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).P1SP 5
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).P1EP 5
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).NPTS 11
#dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).PDLY 0.1

# Initialize Motor positioners horizontal
dbpf $(SCANPREFIX_EMU_H):$(SCAN_SLIT).P4SP -105
dbpf $(SCANPREFIX_EMU_H):$(SCAN_SLIT).P4EP -195
dbpf $(SCANPREFIX_EMU_H):$(SCAN_SLIT).P2SP 1.5
dbpf $(SCANPREFIX_EMU_H):$(SCAN_SLIT).P2EP 1.5
dbpf $(SCANPREFIX_EMU_H):$(SCAN_SLIT).P1SP 5
dbpf $(SCANPREFIX_EMU_H):$(SCAN_SLIT).P1EP 5
dbpf $(SCANPREFIX_EMU_H):$(SCAN_SLIT).NPTS 11
#dbpf $(SCANPREFIX_EMU_V):$(SCAN_SLIT).PDLY 0.1

# Initialize Power supply positioners vertical
########################################## dbpf $(SCANPREFIX_EMU_V):PROC-AngleMax-SP 79
########################################## dbpf $(SCANPREFIX_EMU_V):PROC-AngleMin-SP -79
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_EF).NPTS 200
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_EF).PDLY 0.04
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_EF).DDLY 0.02
########################################## dbpf $(SCANPREFIX_EMU_V):$(SCAN_EF).AAWAIT 1

# Initialize Power supply positioners horizontal
dbpf $(SCANPREFIX_EMU_H):PROC-AngleMax-SP 79
dbpf $(SCANPREFIX_EMU_H):PROC-AngleMin-SP -79
dbpf $(SCANPREFIX_EMU_H):$(SCAN_EF).NPTS 200
dbpf $(SCANPREFIX_EMU_H):$(SCAN_EF).PDLY 0.04
dbpf $(SCANPREFIX_EMU_H):$(SCAN_EF).DDLY 0.02
dbpf $(SCANPREFIX_EMU_H):$(SCAN_EF).AAWAIT 1

dbpf LNS-ISRC-010:PBI-EM:HVver-SP.PREC 4
dbpf LNS-ISRC-010:PBI-EM:HVhor-SP.PREC 4

dbpf $(DAQPREFIX):PBI-EM:CARD0:TriggerEdge 1
dbpf $(DAQPREFIX):PBI-EM:CARD0:NSAMPLES_ms 7
########################################## dbpf $(DAQPREFIX):PBI-EMV:LCURSOR_ms 2
########################################## dbpf $(DAQPREFIX):PBI-EMV:RCURSOR_ms 6
dbpf $(DAQPREFIX):PBI-EMH:LCURSOR_ms 2
dbpf $(DAQPREFIX):PBI-EMH:RCURSOR_ms 6

########################################## dbpf LNS-ISRC-010:PBI-EMV-FC:CurrR:AVG.MDEL -1
########################################## dbpf LNS-ISRC-010:PBI-EMV-FC:CurrR:AVG.ADEL -1
dbpf LNS-ISRC-010:PBI-EMH-FC:CurrR:AVG.MDEL -1
dbpf LNS-ISRC-010:PBI-EMH-FC:CurrR:AVG.ADEL -1
dbpf LNS-ISRC-010:PBI-EM-HV1:VoltR:AVG.MDEL -1
dbpf LNS-ISRC-010:PBI-EM-HV1:VoltR:AVG.ADEL -1

############################################################
################### START ARCHIVE ##########################
############################################################

########################################## dbpf $(ARCHIVE-MACRO-EMUV):PVS "LNS-ISRC-010:PBI-EM-HV1:VoltR,LNS-ISRC-010:PBI-EM-HV2:VoltR,LNS-ISRC-010:PBI-EMV-FC:CurrR,LNS-ISRC-010:PBI-BCM:CurR,LNS-ISRC-010:PBI-EMV:BUFF-MTR-POS")
########################################## dbpf $(ARCHIVE-MACRO-EMUV):Archive 0
dbpf $(ARCHIVE-MACRO-EMUH):PVS "LNS-ISRC-010:PBI-EM-HV1:VoltR,LNS-ISRC-010:PBI-EM-HV2:VoltR,LNS-ISRC-010:PBI-EMH-FC:CurrR,LNS-ISRC-010:PBI-BCM:CurR,LNS-ISRC-010:PBI-EMH:BUFF-MTR-POS")
dbpf $(ARCHIVE-MACRO-EMUH):Archive 0
