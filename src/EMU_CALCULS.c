#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <math.h>
 
/*
* This function calculates both factors converion from V to MRAD and MRAD to V
*/
static int FACTOR_CONVERSION(aSubRecord *precord) {

   	float U = *(float *)precord->a;   // Energy
	float d = *(float *)precord->b;   // Distance between plates
   	float L = *(float *)precord->c;   // Lenght plates
   	float L1 = *(float *)precord->d;  // Face lenght plates
   	float L3 = *(float *)precord->e;  //Rear Lenght plates
   	float factConv_MRADtoVOLT;
	float factConv_VOLTtoMRAD;

	// Factor converion VOLT to MRAD
	factConv_VOLTtoMRAD=0.25*L*(L+2*L3)/(1e-3*U*d*(L1+L+L3));
	*(float *) precord->vala=factConv_VOLTtoMRAD;

	// Factor converion MRAD to VOLT
	factConv_MRADtoVOLT= 4 /((L*(L+2*L3))/((0.001*U*d*(L1+L+L3))));
	 *(float *)precord->valb=factConv_MRADtoVOLT;

	return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(FACTOR_CONVERSION);


/*
* This function calculates the X' (ANGLE) and convert the result WF an SCALARE value(s) in MRAD
*/
static int ANGLE_CALCUL(aSubRecord *precord) {

    float FACT_VtoMRAD = *(float *)precord->a;   // Factor conversion V to MRAD
    float *WF_PS1 = (float *)precord->b;   // Waveform PS1 (VOLT)
    float *WF_PS2 = (float *)precord->c;   // Waveform PS2 (VOLT)
    float AVG_PS1 = *(float *)precord->d;   // PS1 average (VOLT)
    float AVG_PS2 = *(float *)precord->e;  // PS2 average(VOLT)
    int nelm = precord->nova;
    float *pOutA=(float*)(precord->vala);
    int i;
    float val1;
    float val2;

    /* WAVEFORM : Calcul WF_PS1-WF_PS2, and convert the resukt MRAD */
    for (i = 0; i < nelm; ++i)
    {
    *pOutA=((*WF_PS1 - *WF_PS2)* FACT_VtoMRAD)*2000;     // *1000 : to reconvert in mrad
    pOutA++;
    WF_PS1++;
    WF_PS2++;
    }

    /* SCALARE : Calcul AVG_PS1-AVG_PS2, and Convert he result in MRAD*/
    *(float *) precord->valb=((AVG_PS1- AVG_PS2)*FACT_VtoMRAD)*2000;     // *1000 : to reconvert in mrad

  return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(ANGLE_CALCUL);


/*
* This function allows to create the complete waveform for X' (ANGLE) and Y(CURRENT)
*/
static int CREATE_TAB(aSubRecord *precord) {

    float *ptr_WF;
    ptr_WF = (float *)precord->a;   // datas to add

    float *ptr_WF_REENT;
    ptr_WF_REENT = (float *)precord->b;   // Waveform reentrance

    long mtr_cpt = *(long *)precord->c;   //Motor compter
    long nbAngle = *(long *)precord->d;   // Nb Angle
    long nbPosition = *(long *)precord->e;  // Nb position
    long resetBT = *(long *)precord->f;  // BT RESET

    int nelmComp = precord->noa;
    int nelm = precord->nob;

    float *ptr_WF_OUT;
    ptr_WF_OUT = (float*)(precord->vala);

    int i;
    int cpt_for=0.0;

    /***** RESET DATA ******/
    if(resetBT==1)
    {	
    ptr_WF_OUT = (float*)(precord->vala);
      for (i = 0 ; i < nelm; ++i)
        {
        *ptr_WF_OUT=-1.0;
        ptr_WF_OUT++;
	*ptr_WF_REENT=-1.0;
	ptr_WF_REENT++;
        }
    *(long*)(precord->valb)=0;
    return 0;
    }

    /***** REFILL old DATA ******/
    if(mtr_cpt>0)
    {
      cpt_for=mtr_cpt*nbAngle;
    
      for (i = 0 ; i < cpt_for; ++i)
        {
        *ptr_WF_OUT=*ptr_WF_REENT;
        ptr_WF_OUT++;
        ptr_WF_REENT++;
        }
    }
/*******************************************************/
    /***** FILL new DATA ******/
/*    if(mtr_cpt>=0)
    {
      cpt_for=nbPosition-mtr_cpt-1;
      ptr_WF_OUT=(float*)(precord->vala);
      ptr_WF_OUT=ptr_WF_OUT+cpt_for;

      ptr_WF = (float *)precord->a;*/

      /*DATA from the COMPRESS waveform */
/*      for (i = 0 ; i <= nbAngle; ++i)
        {
	*ptr_WF_OUT=*ptr_WF;
        ptr_WF_OUT=ptr_WF_OUT+nbPosition;
        ptr_WF++;
        }
    }*/
    
    /***** FILL new DATA ******/
    if(mtr_cpt>=0)
    {
      ptr_WF_OUT=(float*)(precord->vala)+mtr_cpt;
      ptr_WF = (float *)precord->a;

      /*DATA from the COMPRESS waveform */
      for (i = 0 ; i <= nbAngle; ++i)
        {
	    *ptr_WF_OUT=*ptr_WF;
        ptr_WF_OUT=ptr_WF_OUT+nbPosition;
        ptr_WF++;
        }
    }
  return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(CREATE_TAB);


/*
* This function allows to initiliaze the waveform for X' (ANGLE) and Y(CURRENT)
*/

static int INIT_TAB(aSubRecord *precord) {

     int nelm = precord->nova;
     float *ptr_WF_OUT=(float*)(precord->vala);

    int i;

    for (i = 0 ; i < nelm; ++i)
        {
        *ptr_WF_OUT=-1.0;
        ptr_WF_OUT++;
        }
  return 0;
}

/* Note the function must be registered at the end. */
epicsRegisterFunction(INIT_TAB);

/*
* This function allows to create the complete waveform for X et Y projection
*/
static int CREATE_PROJXY(aSubRecord *precord) {
    /*inputs*/
    float *ptr_angle;
    ptr_angle = (float *)precord->a;   // waveform ANGLE
    float *ptr_current;
    ptr_current = (float *)precord->b;   // Waveform CURRENT
    float *ptr_pos;
    ptr_pos = (float *)precord->c;   // waveform POSITION
    long nbAngle = *(long *)precord->d;   // Nb Angle
    long nbPosition = *(long *)precord->e;  // Nb position

    int nelmComp = precord->noa;
    int nelm = precord->nob;

    int i;
    int u;
    int cpt_for=0.0;
    float sumTemp=0.0;
    float projectionX=0.0;
    float projectionY=0.0;
    float sumCurrentTot=0.0;
    float rms_size=0.0;
    float rms_angle=0.0;
    float rms_correl=0.0;
    float sigma=0.0;

     /***** SUM of current  ******/
     for (i = 0 ; i < nbPosition; i++)
	{
	sumTemp=0.0;
        ptr_current = (float *)precord->b;
	if(i!=0){ptr_current=ptr_current+i;}

	for (u = 0 ; u < nbAngle; u++)
		{
		sumTemp=*ptr_current+sumTemp;
		ptr_current=ptr_current+nbPosition;
		}
	sumCurrentTot=sumCurrentTot+sumTemp;
	}

     /***** X Projection  ******/
     ptr_pos=ptr_pos+nbPosition-1;
     for (i = 0 ; i < nbPosition; i++)
	{
        ptr_current = (float *)precord->b;
        sumTemp=0.0;
	if(i!=0){ptr_current=ptr_current+i;}
	for (u = 0 ; u < nbAngle; u++)
		{
		sumTemp=((*ptr_current)* (*ptr_pos))+sumTemp;
		ptr_current=ptr_current+nbPosition;
		}
	projectionX=projectionX+sumTemp;
	ptr_pos--;
	}
     projectionX=projectionX/sumCurrentTot;
     *(float*)(precord->vala)=projectionX;

     /***** Y Projection  ******/
     ptr_current = (float *)precord->b;
     for (i = 0 ; i < nbAngle; i++)
	{
        sumTemp=0.0;
	for (u = 0 ; u < nbPosition; u++)
		{
		sumTemp=((*ptr_current)* (*ptr_angle))+sumTemp;
		ptr_current++;
		}
	projectionY=projectionY+sumTemp;
	ptr_angle=ptr_angle+nbPosition;
	}
     projectionY=projectionY/sumCurrentTot;
     *(float*)(precord->valb)=projectionY;

     /***** RMS SIZE  ******/
     ptr_current = (float *)precord->b;
     ptr_pos = (float *)precord->c;
     ptr_pos=ptr_pos+nbPosition-1;
     for (i = 0 ; i < nbPosition; i++)
	{
        sumTemp=0.0;
	ptr_current = (float *)precord->b;
	ptr_current=ptr_current+i;
	for (u = 0 ; u < nbAngle; u++)
		{
		sumTemp=(((*ptr_pos-projectionX)*(*ptr_pos-projectionX))*(*ptr_current))+sumTemp;
		ptr_current=ptr_current+nbPosition;
		}
	rms_size=rms_size+sumTemp;
	ptr_pos--;
	}
     rms_size=sqrt(rms_size/sumCurrentTot);
     *(float*)(precord->valc)=rms_size;

     /***** RMS ANGLE  ******/
     ptr_angle = (float *)precord->a; 
     ptr_current = (float *)precord->b;
     for (i = 0 ; i < nbAngle; i++)
	{
        sumTemp=0.0;
	for (u = 0 ; u < nbPosition; u++)
		{
		sumTemp=(((*ptr_angle-projectionY)*(*ptr_angle-projectionY))*(*ptr_current))+sumTemp;
		ptr_current++;
		}
	rms_angle=rms_angle+sumTemp;
	ptr_angle=ptr_angle+nbPosition;
	}
     rms_angle=sqrt(rms_angle/sumCurrentTot);
     *(float*)(precord->vald)=rms_angle;

     /***** RMS CORREL  ******/
     ptr_angle = (float *)precord->a; 
     ptr_current = (float *)precord->b;
     ptr_pos = (float *)precord->c;
     for (i = 0 ; i < nbAngle; i++)
	{
        sumTemp=0.0;
	for (u = 0 ; u < nbPosition; u++)
		{
		sumTemp=(((*ptr_angle-projectionY)*(*ptr_pos-projectionX))*(*ptr_current))+sumTemp;
		ptr_current++;
		}
	rms_correl=rms_correl+sumTemp;
	ptr_angle=ptr_angle+nbPosition;
	ptr_pos--;
	}
     rms_correl=rms_correl/sumCurrentTot;
     *(float*)(precord->vale)=rms_correl;

     /***** SIGMA  ******/
     sigma=fabs(sqrt((rms_size*rms_angle)-(rms_correl*rms_correl)));
     *(float*)(precord->valf)=sigma;
  return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(CREATE_PROJXY);
