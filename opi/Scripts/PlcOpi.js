importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv0 = PVUtil.getString(pvs[0]);

if (pv0 == "home") {
	widget.setPropertyValue("opi_file", "");
	widget.setPropertyValue("opi_file", "/CSS/emu-plc/1-Home.opi");
} else if (pv0 == "fm123") {
	widget.setPropertyValue("opi_file", "");
	widget.setPropertyValue("opi_file", "/CSS/emu-plc/2-FM1,2,3.opi");
} else if (pv0 == "fm45") {
	widget.setPropertyValue("opi_file", "");
	widget.setPropertyValue("opi_file", "/CSS/emu-plc/3-FM4,5.opi");
} else if (pv0 == "InOut") {
	widget.setPropertyValue("opi_file", "");
	widget.setPropertyValue("opi_file", "/CSS/emu-plc/4-Inout_value.opi");
} else {
	pv0 = "home";
}