from org.csstudio.opibuilder.scriptUtil import PVUtil
from os.path import expanduser
import struct
import json
from random import betavariate
import math
import array
import datetime
import time

localtime=time.localtime(time.time())
 
##### VARS #########
type=0
type= PVUtil.getLong(pvs[9])

if type==0:
	NameFile="emittanceVertical"
else:
	NameFile="emittanceHorizontal"

NameFile=NameFile+"_"+str(localtime.tm_year)+"-"+str(localtime.tm_mon)+"-"+str(localtime.tm_mday)+"_"+str(localtime.tm_hour)+"-"+str(localtime.tm_min)+".dat"

NbAngle = 0
NbAngle= PVUtil.getLong(pvs[1])

NbPos = 0
NbPos= PVUtil.getLong(pvs[2])
 
Energie = 0
Energie= PVUtil.getDouble(pvs[3])

Frequence = 0
Frequence=PVUtil.getDouble(pvs[4])

PasMM = 0
PasMM=PVUtil.getDouble(pvs[5])

tabCurr=0
tabCurr=PVUtil.getDoubleArray(pvs[6])

tabAng=0
tabAng=PVUtil.getDoubleArray(pvs[7])

tabPos=0
tabPos=PVUtil.getDoubleArray(pvs[8])

###################################################################################
############################### PLOTWIN ###########################################
###################################################################################

#### OPEN FILE
home = expanduser("~")
PlotFile = open(home+'/data/emit/plotwin/'+NameFile, 'w+')
PlotFile.write("fichier  emittance: X\n")
PlotFile.write("HT ion (kV) : %d\n"%(Energie))
PlotFile.write("frequence (Hz) : %d\n"%(Frequence))
PlotFile.write("N points temps : 1\n")
PlotFile.write("pas de position (mm) : %f\n"%(PasMM))
PlotFile.write("N points  position (mm) : %i\n"%(NbPos))
PlotFile.write("N points angle (mrad) : %i\n"%(NbAngle))
 
## POSITION
try:
	PlotFile.write("vecteur positions (mm) : ")
	for value in range(0,NbPos):
    		PlotFile.write("%.2f "%tabPos[value]);
     		value=value+1
except IndexError :
	pass
	
## ANGLES
PlotFile.write("\nvecteur angles (mrad) : ")
indAngle=0
for value in range(0,NbAngle):
    PlotFile.write("%.2f "%tabAng[indAngle]);
    indAngle=indAngle+NbPos

PlotFile.write("\ntype d'ion : PROTON...")      
PlotFile.write("\ncolonnes de 1 a 4 : indice de temps / indice position / indice angle / I [Volt] :\n")
   
## POSITION/ANGLE/INTENSITY
indCurrent=0
indPos=0
for valuePos in range(0,NbPos):
    for valueAngle in range(0,NbAngle):
            PlotFile.write("0\t%i\t%i\t%.4f\n" %(valuePos,valueAngle,tabCurr[indCurrent]));
            indCurrent=indCurrent+NbPos
    indPos=indPos+1
    indCurrent=indPos

PlotFile.close()


###################################################################################
############################### EXTRACT DATA ######################################
###################################################################################

#### OPEN FILE
PlotFile = open(home+'/data/emit/averageDataEmit/'+"avg-"+NameFile, 'w+')

## POSITION/ANGLE/INTENSITY
indAngle=0
indCurrent=0
indPos=0
try:
	for valuePos in range(0,(NbPos)):	
			for valueAngle in range(0,NbAngle):
    				PlotFile.write("\n%.2f\t\t%.4f\t\t%.8f\n " %(tabPos[valuePos],tabAng[indAngle],tabCurr[indCurrent]));
    				indAngle=indAngle+NbPos
    				indCurrent=indCurrent+NbPos		
			indPos=indPos+1
			indAngle=indPos
			indCurrent=indPos
except IndexError :
	pass
	
PlotFile.close()